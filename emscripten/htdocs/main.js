let songs = [
		"Symphonie/A. Seidel/2nd one on groove.symmod",
		"Composer 670 (CDFM)/CC Catch/amnesia - intro.c67",		
		"Composer 670 (CDFM)/Daredevil/amnesia - credits.c67",		
		"OctaMED MMD0/Alan Mackey/mb-music 07.mmd0",
		"Oktalyzer/Knackosoft/4th exam.8.okta",
		"Protracker/Mahoney/amegas.mod",
		"Screamtracker 3/Abaddon/bleu.s3m",
		"Impulsetracker/Chris Hatfield/sd2 far thunder.it",
		"Mad Tracker 2/Morphine/no more sleep.mt2",
		"Disorder Tracker 2/Statix/calm.plm",
		"Digital Sound Interface Kit RIFF/Boomer/peace or annihilation.dsm",
		"X-Tracker/Vivid/the balance.dmf",
		"Velvet Studio/Demuc/vib-e'n'v.ams",
		"Protracker IFF/Thunder/cosmos.ptm",
	];


class MPTDisplayAccessor extends DisplayAccessor {
	constructor(doGetSongInfo)
	{
		super(doGetSongInfo);
	}

	getDisplayTitle() 		{ return "webMPT";}
	getDisplaySubtitle()	{ return "more tracker music";}
	getDisplayLine1() 
	{ 
		return this.getSongInfo().title + 
				(!this.getSongInfo().artist.length ? "" : (" ("+this.getSongInfo().artist+")"));
	}
	getDisplayLine2() { return this.getSongInfo().tracker; }
	getDisplayLine3() { return ""; }
};


class Main {
	constructor()
	{
		this._backend;
		this._playerWidget;
		this._songDisplay;
	}

	_doOnUpdate()
	{
		if (typeof this._lastId != 'undefined')
		{
			window.cancelAnimationFrame(this._lastId);	// prevent duplicate chains
		}
		this._animate();

		this._songDisplay.redrawSongInfo();
	}

	_animate()
	{
		this._songDisplay.redrawSpectrum();
		this._playerWidget.animate()

		this._lastId = window.requestAnimationFrame(this._animate.bind(this));
	}

	_doOnTrackEnd()
	{
		this._playerWidget.playNextSong();
	}

	_playSongIdx(i)
	{
		this._playerWidget.playSongIdx(i);
	}

	run()
	{
		let preloadFiles = [];	// no need for preload

		// note: with WASM this may not be immediately ready
		this._backend = new MPTBackendAdapter();

		ScriptNodePlayer.initialize(this._backend, this._doOnTrackEnd.bind(this), preloadFiles, true, undefined)
		.then((msg) => {

			let makeOptionsFromUrl = function(someSong) {
					let options= {};

					// drag&dropped temp files start with "/tmp/"
					let isLocal = someSong.startsWith("/tmp/") || someSong.startsWith("music/");
					someSong = isLocal ? someSong : window.location.protocol + "//ftp.modland.com/pub/modules/" + someSong;

					return [someSong, options];
				};

			this._playerWidget = new BasicControls("controls", songs, makeOptionsFromUrl, this._doOnUpdate.bind(this), false, true);

			this._songDisplay = new SongDisplay(new MPTDisplayAccessor((function(){return this._playerWidget.getSongInfo();}.bind(this) )),
								[0xd60000,0xd7d7d7,0xd7d7d7,0x202020], 1, 0.5);

			this._playerWidget.playNextSong();
		});
	}
}