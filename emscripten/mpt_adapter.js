/*
 mpt_adapter.js: Adapts "libopenmpt" backend to generic WebAudio/ScriptProcessor player.

 version 1.1

 	Copyright (C) 2018-2023 Juergen Wothke

 LICENSE
    The code is licensed under the BSD license.
*/
class MPTBackendAdapter extends EmsHEAP16BackendAdapter {
	constructor()
	{
		super(backend_mpt.Module, 2,
				new SimpleFileMapper(backend_mpt.Module),
				new HEAP32ScopeProvider(backend_mpt.Module, 0x8000));

		this._scopeEnabled = false;

		this.ensureReadyNotification();
	}

	enableScope(enable)
	{
		this._scopeEnabled= enable;
	}

	loadMusicData(sampleRate, path, filename, data, options)
	{
		filename = this._getFilename(path, filename);

		let ret = this._loadMusicDataBuffer(filename, data, ScriptNodePlayer.getWebAudioSampleRate(), -999, this._scopeEnabled);

		if (ret == 0)
		{
			this._setupOutputResampling(sampleRate);
		}
		return ret;
	}

	evalTrackOptions(options)
	{
		super.evalTrackOptions(options);

		let id = (options && options.track) ? options.track : 0;
		return this.Module.ccall('emu_set_subsong', 'number', ['number'], [id]);
	}

	getSongInfoMeta()
	{
		return {
			title: String,
			artist: String,
			type: String,
			msg: String,
			tracker: String,
			tracks: String,
		};
	}

	updateSongInfo(filename)
	{
		let result = this._songInfo;

		let numAttr = 6;
		let ret = this.Module.ccall('emu_get_track_info', 'number');

		let array = this.Module.HEAP32.subarray(ret>>2, (ret>>2)+numAttr);

		result.title = this.Module.UTF8ToString(array[0]);
		if (!result.title.length) result.title = filename.replace(/^.*[\\\/]/, '');

		result.artist = this.Module.UTF8ToString(array[1]);
		result.type = this.Module.UTF8ToString(array[2]);
		result.msg = this.Module.UTF8ToString(array[3]);
		result.tracker = this.Module.UTF8ToString(array[4]);
		result.tracks = this.Module.UTF8ToString(array[5]);
	}

};
