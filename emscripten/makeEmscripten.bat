::  POOR MAN'S DOS PROMPT BUILD SCRIPT.. make sure to delete the respective built/*.bc files before re-building!
::  existing *.bc files will not be recompiled. 
::  had to split the below to workaround DOS command length limitations..

setlocal enabledelayedexpansion

SET ERRORLEVEL
VERIFY > NUL

:: **** use the "-s WASM" switch to compile WebAssembly output. warning: the SINGLE_FILE approach does NOT currently work in Chrome 63.. ****

set "OPT_BASE= -s WASM=1 -Wall -Wextra -Wpedantic -s DEMANGLE_SUPPORT=0 -s ASSERTIONS=0 -s FORCE_FILESYSTEM=1 -fPIC -s PRECISE_F32=1 -s ERROR_ON_UNDEFINED_SYMBOLS=1 -Wcast-align -fno-strict-aliasing -s VERBOSE=0 -s SAFE_HEAP=0 -s DISABLE_EXCEPTION_CATCHING=0 -D_LITTLE_ENDIAN -DHAVE_STDINT_H -DNO_DEBUG_LOGS -Wno-pointer-sign -I. -I.. -I../src -I../src/src -I../src/include  -I../src/common -I../src/soundbase -Os -O3 "
set "OPTMPT= -DMPT_COMPILER_CLANG -DLIBOPENMPT_BUILD -DLIBOPENMPT_NO_DEPRECATE -DOPENMPT_VERSION_REVISION="130" -DMPT_CHECK_CXX_IGNORE_WARNING_FINITEMATH -DMPT_CHECK_CXX_IGNORE_WARNING_FASTMATH %OPT_BASE%"

set "INCH= -I../src/include/zlib -I../src/include/vorbis/include -I../src/include/mpg123/ports/makefile -I../src/include/mpg123/src/libmpg123  -I../src/include/ogg/include"
set "OPTI=  -DMPT_WITH_ZLIB -DMPT_WITH_VORBISFILE -DMPT_WITH_MPG123 -DMPT_WITH_VORBIS -DMPT_WITH_OGG -DHAVE_ALLOCA_H -DOPT_GENERIC -DSTDC -DZ_HAVE_UNISTD_H -ffast-math %INCH% -I../src/include/vorbis/lib -I../src/include/ogg/ports/makefile -I../src/include/mpg123/src/compat -I../src/include/mpg123/src  %OPTX%"
set "OPTXX= -std=c++17 -DMPT_WITH_ZLIB -DMPT_WITH_VORBISFILE -DMPT_WITH_MPG123 -DMPT_WITH_VORBIS -DMPT_WITH_OGG -ffast-math %OPTMPT%"

set "OPTLNK= %OPTXX% -s TOTAL_MEMORY=67108864 --closure 1 --llvm-lto 1 --memory-init-file 0"

if not exist "built/includes.bc" (
	call emcc.bat %OPTI%  ../src/include/zlib/adler32.c ../src/include/zlib/compress.c ../src/include/zlib/crc32.c ../src/include/zlib/deflate.c ../src/include/zlib/gzclose.c ../src/include/zlib/gzlib.c ../src/include/zlib/gzread.c ../src/include/zlib/gzwrite.c ../src/include/zlib/infback.c ../src/include/zlib/inffast.c ../src/include/zlib/inflate.c ../src/include/zlib/inftrees.c ../src/include/zlib/trees.c ../src/include/zlib/uncompr.c ../src/include/zlib/zutil.c ../src/include/ogg/src/bitwise.c ../src/include/ogg/src/framing.c ../src/include/vorbis/lib/analysis.c ../src/include/vorbis/lib/bitrate.c ../src/include/vorbis/lib/block.c ../src/include/vorbis/lib/codebook.c ../src/include/vorbis/lib/envelope.c ../src/include/vorbis/lib/floor0.c ../src/include/vorbis/lib/floor1.c ../src/include/vorbis/lib/info.c ../src/include/vorbis/lib/lookup.c ../src/include/vorbis/lib/lpc.c ../src/include/vorbis/lib/lsp.c ../src/include/vorbis/lib/mapping0.c ../src/include/vorbis/lib/mdct.c ../src/include/vorbis/lib/psy.c ../src/include/vorbis/lib/registry.c ../src/include/vorbis/lib/res0.c ../src/include/vorbis/lib/sharedbook.c ../src/include/vorbis/lib/smallft.c ../src/include/vorbis/lib/synthesis.c ../src/include/vorbis/lib/vorbisenc.c ../src/include/vorbis/lib/vorbisfile.c ../src/include/vorbis/lib/window.c ../src/include/mpg123/src/compat/compat.c ../src/include/mpg123/src/compat/compat_str.c ../src/include/mpg123/src/libmpg123/dct64.c ../src/include/mpg123/src/libmpg123/equalizer.c ../src/include/mpg123/src/libmpg123/feature.c ../src/include/mpg123/src/libmpg123/format.c ../src/include/mpg123/src/libmpg123/frame.c ../src/include/mpg123/src/libmpg123/icy.c ../src/include/mpg123/src/libmpg123/icy2utf8.c ../src/include/mpg123/src/libmpg123/id3.c ../src/include/mpg123/src/libmpg123/index.c ../src/include/mpg123/src/libmpg123/layer1.c ../src/include/mpg123/src/libmpg123/layer2.c ../src/include/mpg123/src/libmpg123/layer3.c ../src/include/mpg123/src/libmpg123/libmpg123.c ../src/include/mpg123/src/libmpg123/ntom.c ../src/include/mpg123/src/libmpg123/optimize.c ../src/include/mpg123/src/libmpg123/parse.c ../src/include/mpg123/src/libmpg123/readers.c ../src/include/mpg123/src/libmpg123/stringbuf.c ../src/include/mpg123/src/libmpg123/synth.c ../src/include/mpg123/src/libmpg123/synth_8bit.c ../src/include/mpg123/src/libmpg123/synth_real.c ../src/include/mpg123/src/libmpg123/synth_s32.c ../src/include/mpg123/src/libmpg123/tabinit.c -r -o built/includes.bc 
	IF !ERRORLEVEL! NEQ 0 goto :END
)

if not exist "built/common.bc" (
	call emcc.bat %OPTXX% ../src/common/ComponentManager.cpp ../src/common/Logging.cpp ../src/common/mptFileIO.cpp ../src/common/mptFileTemporary.cpp ../src/common/mptFileType.cpp ../src/common/mptPathString.cpp ../src/common/mptRandom.cpp ../src/common/mptString.cpp ../src/common/mptStringBuffer.cpp ../src/common/mptTime.cpp ../src/common/Profiler.cpp ../src/common/serialization_utils.cpp ../src/common/version.cpp -r -o built/common.bc
	IF !ERRORLEVEL! NEQ 0 goto :END
)
if not exist "built/sounddsp.bc" (
	call emcc.bat%OPTXX% ../src/sounddsp/AGC.cpp ../src/sounddsp/DSP.cpp ../src/sounddsp/EQ.cpp ../src/sounddsp/Reverb.cpp -r -o built/sounddsp.bc
	IF !ERRORLEVEL! NEQ 0 goto :END
)

if not exist "built/plugins.bc" (
	call emcc.bat %OPTXX% ../src/soundlib/plugins/DigiBoosterEcho.cpp ../src/soundlib/plugins/LFOPlugin.cpp ../src/soundlib/plugins/PluginManager.cpp ../src/soundlib/plugins/PlugInterface.cpp ../src/soundlib/plugins/SymMODEcho.cpp ../src/soundlib/plugins/dmo/Chorus.cpp ../src/soundlib/plugins/dmo/Compressor.cpp ../src/soundlib/plugins/dmo/Distortion.cpp ../src/soundlib/plugins/dmo/DMOPlugin.cpp ../src/soundlib/plugins/dmo/DMOUtils.cpp ../src/soundlib/plugins/dmo/Echo.cpp ../src/soundlib/plugins/dmo/Flanger.cpp ../src/soundlib/plugins/dmo/Gargle.cpp ../src/soundlib/plugins/dmo/I3DL2Reverb.cpp ../src/soundlib/plugins/dmo/ParamEq.cpp ../src/soundlib/plugins/dmo/WavesReverb.cpp -r -o built/plugins.bc
	IF !ERRORLEVEL! NEQ 0 goto :END
)
if not exist "built/soundlib1.bc" (
	call emcc.bat %OPTXX% %INCH%  ../src/soundlib/UpgradeModule.cpp ../src/soundlib/AudioCriticalSection.cpp ../src/soundlib/ContainerMMCMP.cpp  ../src/soundlib/ContainerPP20.cpp ../src/soundlib/ContainerUMX.cpp ../src/soundlib/ContainerXPK.cpp ../src/soundlib/Dlsbank.cpp ../src/soundlib/Fastmix.cpp ../src/soundlib/InstrumentExtensions.cpp ../src/soundlib/ITCompression.cpp ../src/soundlib/ITTools.cpp ../src/soundlib/Message.cpp ../src/soundlib/MIDIEvents.cpp ../src/soundlib/MIDIMacros.cpp ../src/soundlib/MixerLoops.cpp ../src/soundlib/MixerSettings.cpp ../src/soundlib/MixFuncTable.cpp ../src/soundlib/mod_specifications.cpp ../src/soundlib/ModChannel.cpp ../src/soundlib/modcommand.cpp ../src/soundlib/ModInstrument.cpp ../src/soundlib/ModSample.cpp ../src/soundlib/ModSequence.cpp ../src/soundlib/modsmp_ctrl.cpp ../src/soundlib/MPEGFrame.cpp ../src/soundlib/OggStream.cpp ../src/soundlib/OPL.cpp ../src/soundlib/pattern.cpp ../src/soundlib/patternContainer.cpp ../src/soundlib/Paula.cpp ../src/soundlib/RowVisitor.cpp ../src/soundlib/S3MTools.cpp ../src/soundlib/SampleFormatBRR.cpp ../src/soundlib/SampleFormatFLAC.cpp ../src/soundlib/SampleFormatMediaFoundation.cpp ../src/soundlib/SampleFormatMP3.cpp ../src/soundlib/SampleFormatOpus.cpp ../src/soundlib/SampleFormats.cpp ../src/soundlib/SampleFormatSFZ.cpp ../src/soundlib/SampleFormatVorbis.cpp ../src/soundlib/SampleIO.cpp ../src/soundlib/Snd_flt.cpp ../src/soundlib/Snd_fx.cpp ../src/soundlib/Sndfile.cpp ../src/soundlib/Sndmix.cpp ../src/soundlib/SoundFilePlayConfig.cpp ../src/soundlib/Tagging.cpp ../src/soundlib/TinyFFT.cpp ../src/soundlib/tuning.cpp ../src/soundlib/tuningCollection.cpp ../src/soundlib/UMXTools.cpp ../src/soundlib/WAVTools.cpp ../src/soundlib/WindowedFIR.cpp ../src/soundlib/XMTools.cpp -r -o built/soundlib1.bc
	IF !ERRORLEVEL! NEQ 0 goto :END
)
if not exist "built/soundlib2.bc" (
	call emcc.bat %OPTXX% %INCH% ../src/soundlib/Tables.cpp ../src/soundlib/Load_669.cpp ../src/soundlib/Load_amf.cpp ../src/soundlib/Load_ams.cpp ../src/soundlib/Load_c67.cpp ../src/soundlib/Load_dbm.cpp ../src/soundlib/Load_digi.cpp ../src/soundlib/Load_dmf.cpp ../src/soundlib/Load_dsm.cpp ../src/soundlib/Load_dsym.cpp ../src/soundlib/Load_dtm.cpp ../src/soundlib/Load_far.cpp ../src/soundlib/Load_fmt.cpp ../src/soundlib/Load_gdm.cpp ../src/soundlib/Load_gt2.cpp ../src/soundlib/Load_imf.cpp ../src/soundlib/Load_it.cpp ../src/soundlib/Load_itp.cpp ../src/soundlib/load_j2b.cpp ../src/soundlib/Load_mdl.cpp ../src/soundlib/Load_med.cpp ../src/soundlib/Load_mid.cpp ../src/soundlib/Load_mo3.cpp ../src/soundlib/Load_mod.cpp ../src/soundlib/Load_mt2.cpp ../src/soundlib/Load_mtm.cpp ../src/soundlib/Load_mus_km.cpp  -r -o built/soundlib2.bc
	IF !ERRORLEVEL! NEQ 0 goto :END
)
if not exist "built/soundlib3.bc" (
	call emcc.bat %OPTXX% %INCH% ../src/soundlib/Load_okt.cpp ../src/soundlib/Load_plm.cpp ../src/soundlib/Load_psm.cpp ../src/soundlib/Load_ptm.cpp ../src/soundlib/Load_s3m.cpp ../src/soundlib/Load_sfx.cpp ../src/soundlib/Load_stm.cpp ../src/soundlib/Load_stp.cpp ../src/soundlib/Load_symmod.cpp ../src/soundlib/Load_uax.cpp ../src/soundlib/Load_ult.cpp ../src/soundlib/Load_wav.cpp ../src/soundlib/Load_xm.cpp  -r -o built/soundlib3.bc
	IF !ERRORLEVEL! NEQ 0 goto :END
)
if not exist "built/libopenmpt.bc" (
	call emcc.bat %OPTXX% ../src/libopenmpt/libopenmpt_impl.cpp ../src/libopenmpt/libopenmpt_c.cpp ../src/libopenmpt/libopenmpt_ext_impl.cpp ../src/libopenmpt/libopenmpt_cxx.cpp -r -o built/libopenmpt.bc
	IF !ERRORLEVEL! NEQ 0 goto :END
)

call emcc.bat %OPTLNK% built/includes.bc built/plugins.bc built/common.bc built/sounddsp.bc built/soundlib1.bc built/soundlib2.bc built/soundlib3.bc built/libopenmpt.bc MetaInfoHelper.cpp Adapter.cpp  -s EXPORTED_FUNCTIONS="['_emu_load_file','_emu_teardown','_emu_get_current_position','_emu_seek_position','_emu_get_max_position','_emu_set_subsong','_emu_get_track_info','_emu_get_sample_rate','_emu_get_audio_buffer','_emu_get_audio_buffer_length', '_emu_compute_audio_samples','_emu_number_trace_streams','_emu_get_trace_streams', '_malloc', '_free']"  -o htdocs/mpt.js  -s SINGLE_FILE=0 -s EXTRA_EXPORTED_RUNTIME_METHODS="['ccall', 'UTF8ToString']"  -s BINARYEN_ASYNC_COMPILATION=1  && copy /b shell-pre.js + htdocs\mpt.js + shell-post.js htdocs\web_mpt3.js && del htdocs\mpt.js && copy /b htdocs\web_mpt3.js + mpt_adapter.js htdocs\backend_mpt.js && del htdocs\web_mpt3.js
:END







