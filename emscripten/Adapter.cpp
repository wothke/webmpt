/*
* This file adapts "libopenmpt" to the interface expected by my generic JavaScript player..
*
* Copyright (C) 2018-2023 Juergen Wothke
*
* LICENSE
*
* The code is licensed under the BSD license.
*/

#include <emscripten.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>     /* malloc, free, rand */

#include <exception>
#include <iostream>
#include <fstream>

#include "MetaInfoHelper.h"
using emsutil::MetaInfoHelper;

extern "C" int32_t** getScopeBuffers(); 	// hack: see Sndfile.cpp
extern "C" int getUsedScopeChannels();

#include "../src/libopenmpt/libopenmpt.h"
#include "../src/libopenmpt/libopenmpt.hpp"


#define MIXBUFFERSIZE 512	// MPT's mixbuffer size (use same here to avoid copying "scope" buffers.)
#define SAMPLE_BUF_SIZE	MIXBUFFERSIZE

#define CHANNELS 2

namespace mpt {

	class Adapter {
	public:
		Adapter() : _module(NULL), _fileBuf(NULL), _sampleRate(0), _samplesAvailable(0)
		{
		}

		int loadFile(char *filename, void * inBuffer, uint32_t inBufSize, uint32_t sampleRate, uint32_t audioBufSize, uint32_t scopesEnabled)
		{
			// fixme: scopes are currently always enabled

			_sampleRate = sampleRate;

			_fileBuf = malloc(inBufSize);
			memcpy(_fileBuf, inBuffer, inBufSize);

			// openmpt_log_func_default / openmpt_log_func_silent
			_module = openmpt_module_create_from_memory( (const void *) _fileBuf, inBufSize, openmpt_log_func_default, NULL, NULL );

			return _module ? 0 : 1;
		}

		int setSubsong(int subsong)
		{

			if (subsong >= 0)
			{
				openmpt_module_select_subsong( _module, subsong );
			}
			updateSongInfo();

			return 0;
		}

		void teardown()
		{
			if (_fileBuf)
			{
				free(_fileBuf);
				_fileBuf = NULL;
			}

			if (_module)
			{
				openmpt_module_destroy(_module);
				_module = NULL;
			}
		}

		int getSampleRate()
		{
			return _sampleRate;
		}

		int genSamples()
		{
			_samplesAvailable = 0;

			if (_module)
			{
				_samplesAvailable = openmpt_module_read_interleaved_stereo( _module, _sampleRate, SAMPLE_BUF_SIZE, (int16_t *) _sampleBuffer );
			}

			return _samplesAvailable == 0 ? 1 : 0;
		}

		char* getSampleBuffer()
		{
			return (char*)_sampleBuffer;
		}

		long getSampleBufferLength()
		{
			return _samplesAvailable;
		}

		int getCurrentPosition()
		{
			return _module ? openmpt_module_get_position_seconds( _module ) * 1000 : 0;
		}

		void seekPosition(int ms)
		{
			if (_module)
			{
				openmpt_module_set_position_seconds( _module, ((double)ms) / 1000 );
			}
		}

		int getMaxPosition()  // ms
		{
			return _module ? openmpt_module_get_duration_seconds( _module ) * 1000 : -1;
		}

		int getNumberTraceStreams()
		{
			return getUsedScopeChannels();
		}

		const char** getTraceStreams()
		{
			return (const char**)(_module ? getScopeBuffers() : 0);
		}
	private:
		void updateSongInfo()
		{
			MetaInfoHelper::getInstance()->clear();

			if (_module)
			{
				int32_t subsong = openmpt_module_get_selected_subsong( _module );

				MetaInfoHelper *info = MetaInfoHelper::getInstance();
				info->setText(0, openmpt_module_get_metadata( _module, "title" ), "");
				info->setText(1, openmpt_module_get_metadata( _module, "artist" ), "");
				info->setText(2, openmpt_module_get_metadata( _module, "type_long" ), "");
				info->setText(3, openmpt_module_get_metadata( _module, "message" ), "");
				info->setText(4, openmpt_module_get_metadata( _module, "tracker" ), "");

				std::string n = std::to_string(openmpt_module_get_num_subsongs( _module ));
				info->setText(5, n.c_str(), "");
			}
		}
	private:
		openmpt_module*		_module;
		void*				_fileBuf;

		uint32_t			_sampleRate;
		int					_samplesAvailable;
		int16_t				_sampleBuffer[SAMPLE_BUF_SIZE * CHANNELS];

	};
};

mpt::Adapter _adapter;


// old style EMSCRIPTEN C function export to JavaScript.
// todo: code might be cleaned up using EMSCRIPTEN's "new" Embind feature:
// https://emscripten.org/docs/porting/connecting_cpp_and_javascript/embind.html
#define EMBIND(retType, func)  \
	extern "C" retType func __attribute__((noinline)); \
	extern "C" retType EMSCRIPTEN_KEEPALIVE func


EMBIND(int, emu_load_file(char *filename, void * inBuffer, uint32_t inBufSize, uint32_t sampleRate, uint32_t audioBufSize, uint32_t scopesEnabled)) {
	return _adapter.loadFile(filename, inBuffer, inBufSize, sampleRate, audioBufSize, scopesEnabled); }
EMBIND(void, emu_teardown())						{ _adapter.teardown(); }
EMBIND(int, emu_get_sample_rate())					{ return _adapter.getSampleRate(); }
EMBIND(int, emu_set_subsong(int track))				{ return _adapter.setSubsong(track); }
EMBIND(const char**, emu_get_track_info())			{ return MetaInfoHelper::getInstance()->getMeta(); }
EMBIND(int, emu_compute_audio_samples())			{ return _adapter.genSamples(); }
EMBIND(char*, emu_get_audio_buffer())				{ return _adapter.getSampleBuffer(); }
EMBIND(int, emu_get_audio_buffer_length())			{ return _adapter.getSampleBufferLength(); }
EMBIND(int, emu_get_current_position())				{ return _adapter.getCurrentPosition(); }
EMBIND(void, emu_seek_position(int ms))				{ _adapter.seekPosition(ms);}
EMBIND(int, emu_get_max_position())					{ return _adapter.getMaxPosition(); }
EMBIND(int, emu_number_trace_streams())				{ return _adapter.getNumberTraceStreams(); }
EMBIND(const char**, emu_get_trace_streams())		{ return _adapter.getTraceStreams(); }
